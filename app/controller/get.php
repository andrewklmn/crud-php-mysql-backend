<?php

    /*
        TODO - 
        http://localhost/tables/ - get list of tables
        http://localhost/table/ - get all records of table 
        http://localhost/table?limit=100&filter=id>100 
                                   - get all records with limit and filter      * 
        http://localhost/table/:id - get record with ID
     
    */

    if ($table=='') {
        $data = array();
        $tables = $pdo->query("SHOW TABLES;")->fetchAll(PDO::FETCH_NUM);
        foreach ($tables as $next_table) {
            $data[] = $next_table[0];
        };
        header("HTTP/1.1 200 OK");
        echo json_encode($data);
    } else {
        
        if ($id=='') {
            $limit = 100;
            $filter = '';
            if (isset($_REQUEST['limit']) AND (int)$_REQUEST['limit']>0) {
                $limit = (int)$_REQUEST['limit'];
            };
            if (isset($_REQUEST['filter']) AND $_REQUEST['filter']!='') {
                $filter = ' WHERE '.$_REQUEST['filter'];
            };
            $sql = 'SELECT * FROM '.addslashes($table).' '.$filter.'  LIMIT 0,'.$limit.';';
            $data = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            header("HTTP/1.1 200 OK");
            echo json_encode($data);
            //print_array_in_pre_html($data);
        } else {
            $data = $pdo->query('SELECT * FROM '.addslashes($table).' WHERE id="'.$id.'";')->fetchAll(PDO::FETCH_ASSOC);
            if(isset($data[0])) {
                header("HTTP/1.1 200 OK");
                echo json_encode($data[0]);
            } else {
                header("HTTP/1.0 404 Not Found");
                echo json_encode(array(
                    'error' => 'Not Found'
                ));
            };
            //print_array_in_pre_html($data[0]);
        };
        
    };
    
    exit();
    
    //header('HTTP/1.0 400 Bad Request');
