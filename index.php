<?php


//    use Kreait\Firebase\Factory;
//    use Kreait\Firebase\ServiceAccount;

    //include './app/redirect_to_https.php';
    include './app/controller/check_auth.php';
    include './config/db.php';
    include './app/view/print_array_in_pre_html.php';
    include './app/controller/parse_url.php';
    //echo '<h1>',$_SERVER['REQUEST_METHOD'],'</h1>';
    
    
    
    
    switch ($_SERVER['REQUEST_METHOD']){
        case 'GET':
            include './app/controller/get.php';
            break;
        case 'PUT':
            break;
        case 'POST':
            break;
        case 'DELETE':
            break;
        default:
            header('HTTP/1.0 400 Bad Request');
            echo json_encode(array(
                'error' => 'Bad Request'
            ));
            exit();
            break;
    };
