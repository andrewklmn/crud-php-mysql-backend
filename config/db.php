<?php

/* 
 * DB connection
 */
if (!isset($pdo)) {
    
$servername = "localhost";
$username = "andrew";
$password = "reversi";
$database = 'renters';

try {
    $pdo = new PDO(
            "mysql:host=$servername;dbname=$database", 
            $username, 
            $password,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    // set the PDO error mode to exception
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected successfully";
    }
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }
};